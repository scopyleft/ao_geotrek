
https://github.com/GeotrekCE

http://geotrek.fr/

- Lot 1 : Développements de Geotrek-Admin
- Lot 2 : Refonte du portail Geotrek-Rando
- Lot 3 : Développements de l'application Geotrek-Mobile

Max 349 385 € TTC

En cas de groupement, la forme souhaitée par le pouvoir adjudicateur est un
groupement conjoint avec mandataire solidaire.

le projet bénéficie de crédits européens et nationaux qui nécessitent des
délais de livraison impératifs qui seront précisés chaque année aux
prestataires.


Date et heure limite de réception des candidatures 30/09/2019 à 12h00


Le prestataire candidat rédigera une note technique tenant compte du Cahier des
Clauses Techniques Particulière et démontrant la vision du prestataire du
projet ainsi que la bonne compréhension de la demande, des livrables, et du
phasage. Elle précisera l'approche méthodologique envisagée. La note détaillera
également l'approche de gestion de projet qui sera proposée par lot et pour la
coordination entre les 3 lots, les process de validation et les outils de suivi
de projets envisagés. Le prestataire précisera les savoir-faire des personnes
susceptibles d'intervenir sur le projet, joindra leurs Diplômes, joindra leurs
CV, et éventuellement ceux des sous-traitants qui travailleront sur le projet,
ainsi que les modalités d’interventions et d'échanges au cours de la mission.
Enfin, il détaillera les références/projets déjà réalisés dans des domaines
similaires.

Critères de jugement des candidatures : Le jugement des candidatures se fera
sur la valeur technique selon les items suivants :
- La compréhension technique et méthodologique du projet par lot et global,
  ainsi que la dimension opensource : 40 %
- La méthodologie de gestion du projet : 30 %
- La qualité des références, les capacités du candidat et les moyens dédiés au
  projet : 30 %


Date et heure limite de réception des offres initiales des candidats 18/10/2019 à 12h00

Devis et estimations

Critères de jugement des offres :
Les critères retenus pour le jugement des offres sont indiqués ci-dessous et pondérés de la manière
suivante :
- Prix comparé selon les devis quantitatifs estimatifs (40%)
- Valeur technique (60%) répartie de la manière suivante :
  - La qualité des propositions techniques de la note méthodologique explicative du DQE : 60 %
  - Les capacités du candidat : 20 %
  - La robustesse du plan de tests et de recettage : 20 %



Enjeux :

Lot1 : perf. aujourd'hui, ça limite l'extension du produit

Le principal axe de la commande est l’intégration des activités outdoor au sens large.

De nouvelles informations devront être implémentées ce qui aura pour effet de :
- faire évoluer le modèle de données actuel,
- adapter l’interface graphique du back office, Geotrek-admin, afin de mieux organiser l’ensemble des fonctionnalités,
- de retravailler l’ergonomie du portail Geotrek-rando pour intégrer les nouveaux contenus.

Les activités outdoor développées sont les suivantes :
- les activités verticales (escalade, via-ferrata, alpinisme et le canyoning),
- les activités d’eau vive,
- le parapente.

Ces activités feront l’objet d’un module à part entière qui sera intégré dans Geotrek.

:falcepalm:

Un groupe de travail constitué d’utilisateurs de Geotrek et de gestionnaires, a travaillé sur la spécification de leurs besoins en terme de gestion et de valorisation de ces pratiques. Le résultat de ces sessions de travail est traduit sous la forme d’un modèle conceptuel de données (MCD) explicité dans le lot Geotrek-admin.


PMR
Ces évolutions auront un impact sur les 3 outils de la plateforme :
- Geotrek-admin : L’ajout de champs spécifiques dans un onglet “accessibilité”.
- Geotrek-rando / Geotrek-mobile : Prise en compte de ces informations dans l’affichage des objets et création d’un accès direct “accessibilité”.



Lot1

Le prestataire animera notamment un atelier (durée à définir) avec le groupe de travail technique permettant de faire préciser aux clients les besoins et les livrables attendus.

Si besoin, les ateliers seront découpés en 3 : technique, thématique et fonctionnel.




Il est souhaité renforcer ce fonctionnement et organiser les pratiques de développements avec

**“Une fonctionnalité = 1 issue (ticket), une branche, une pull request, des tests unitaires, de la documentation”.**

Un grand nombre de corrections ou fonctionnalités ont déjà été identifiées. Ils sont annexés à ce document ou listés dans les dépôts Github, à compléter. Un travail de catégorisation a été amorcé sur le dépôt de Geotrek-admin, dans le projet Github GC-2019 qui sera complété et utilisé pour le projet.



